<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ExcelExport implements FromView
{

	public $result;

	public function __construct($result)
    {
        $this->result = $result;
    }

    public function view(): View
    {
        return view('welcome', [
            'result' => $this->result,
            'err' => null,
        ]);
    }
}