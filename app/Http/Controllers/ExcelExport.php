<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use \Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;


class ExcelExport implements FromView, ShouldAutoSize, WithEvents
{

	 use  RegistersEventListeners;

	public static $result;



	public function __construct($result)
    {
        self::$result = $result;
    }

    public static function afterSheet(AfterSheet $event)
    {

    	$event->sheet->styleCells(
	                    'E1:E50',
	                    [
	                    	'alignment' => [ 
	                    		'wrapText' => 'true',
	                    	],
	                    ]
	                );

    	// $event->sheet->setRowHeight('5','100');
    	// $event->sheet->setRowHeight('7','100');
    	$event->sheet->setColWidth('E','70');


    	$i = 2;
    	foreach (self::$result as $key => $value) {
    		
    		$cell = 'C'. $i .':C'. ($i+1);

    		if (!is_bool($value)) {
    			continue;
    		}

    		if ($value === true) {
    			$styleArray = [
    				'fill' => [
			        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
			        'color' => [
			            'argb' => 'FF90EE90',
			        ],

			        
			    ]
			];
    		}
    		else{
    			$styleArray = [
    				'fill' => [
			        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
			        'color' => [
			            'argb' => 'FF8B0000',
			        ],
			        
			    ]



			];

			if ($key == 'hostCount' || $key == 'hasHost') {
			    	$event->sheet->setRowHeight((string)$i+1,'100');
			    }

var_dump($key . ' ' . $value);
    		}


    	
	        $event->sheet->styleCells(
	                    $cell,
	                    $styleArray
	                );
	        $i+=2;
    	}
    }


    public function view(): View
    {	

    

        return view('result', [
            'result' => self::$result,
            'err' => null,
        ]);
    }
}