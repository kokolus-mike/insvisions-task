<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\ExcelExport;
use Illuminate\Support\Facades\Storage;

class MainController extends Controller
{
	public $result;

	public function __construct()
    {
        $this->result =  (object) array();
    }

	public function index(){
		return view('welcome',[
			'err' => 'Input some URL',
			'result' => null,

		]);
	}

    public function analyzeUrl(Request $request){

    	$this->validate($request, [
	        'url' => 'required|url',
	    ]);

    	$url =  $request->url;

    	try {
        	$fp = fopen($url, 'r');
	    } 
	    catch (\Exception $e) {
	        report($e);
	        return view('welcome',[
	        	'err' => 'URL adress is invalid',
				'result' => $this->result,
			]);
	    }

		$meta_data = stream_get_meta_data($fp);

		foreach ($meta_data['wrapper_data'] as $response) {
		    if (strtolower(substr($response, 0, 10)) == 'location: ') {
		        $url = substr($response, 10);
		    }
		}

		$this->result->fileExists = true;

		try {
        	$contents = file_get_contents ($url . 'robots.txt');
	    } 
	    catch (\Exception $e) {
	        report($e);
	        $this->result->fileExists = false;
	    }

	    if ($this->result->fileExists){

			$this->result->hasHost = substr_count(strtolower($contents), 'host') ? true : false;

			$this->result->hostCount = substr_count(strtolower($contents), 'host') == 1 ? true : false;

			$this->result->response = $http_response_header[0]; 

			 $filesize = false;

		    foreach ($http_response_header as $response) {
		    	 if (strtolower(substr($response, 0, 16)) == 'content-length: ') {
		    	 	$this->result->size = (int)substr($response, 16);
		    	 	$this->result->sizeCorrect = (int)substr($response, 16) < 32768 ? true : false;
		    	 	$filesize = true;
		    	 }
		    	 
		    }

		    if (!$filesize) {
		    	$this->result->sizeCorrect = false;
		    	$this->result->size = 'Cant get filesize';
		    }   

		    $this->result->hasSitemap = substr_count(strtolower($contents), 'sitemap') ? true : false;

		    $this->result->responseCorrect = strtolower(substr(strrev($http_response_header[0]), 0, 2)) == 'ko' ? true :false;





		}



		Excel::store(new ExcelExport($this->result), 'result.xlsx');



		return view('welcome',[
			'err' => null,
			'result' => $this->result,

		]); 
    }

    public function download() {
    	
	    return Storage::download('result.xlsx');
	}

}
