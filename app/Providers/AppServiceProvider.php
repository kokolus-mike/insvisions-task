<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use \Maatwebsite\Excel\Sheet;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        }); 

        Sheet::macro('setRowHeight', function (Sheet $sheet, string $rowDimension, string $hight) {
            $sheet->getDelegate()->getRowDimension($rowDimension)->setRowHeight($hight);
        }); 

        Sheet::macro('setColWidth', function (Sheet $sheet, string $columnDimension, string $width) {
            $sheet->getDelegate()->getColumnDimension($columnDimension)->setAutoSize(false);
            $sheet->getDelegate()->getColumnDimension($columnDimension)->setWidth($width);
        }); 
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
