<div class="table-responsive" style="font-weight: bold;">          
  <table class="table">
    <thead>
      <tr>
        <th>№</th>
        <th>Название проверки</th>
        <th>Статус</th>
        <th> </th>
        <th>Текущее состояние</th>
      </tr>
    </thead>
    <tbody>
    	<tr>
    		<td rowspan="2">1</td>
    		<td rowspan="2">Проверка наличия файла robots.txt</td>
    		@if ($result->fileExists)
    			<td rowspan="2" style="background-color: green; color: yellow">OK</td>
    			<td>Состояние</td>
    			<td>Файл robots.txt присутствует</td>
				</tr>
				<tr>
					<td>Рекомендации</td>
					<td>Доработки не требуются</td>
				</tr>
    		@else
				<td rowspan="2" style="background-color: red; color: black">ОШИБКА</td>
				<td>Состояние</td>
    			<td>Файл robots.txt отсутствует</td>
				</tr>
				<tr>
					<td>Рекомендации</td>
					<td>Программист: Создать файл robots.txt и разместить его на сайте.</td>
				</tr>
			@endif
			{{-- <tr>
				@for ($i = 0; $i < 5; $i++)
				    <td rowspan="2" style="background-color: gray;"></td>
				@endfor
			</tr> --}}

			@if($result->fileExists)

			<tr>
    		<td rowspan="2">6</td>
    		<td rowspan="2">Проверка указания директивы Host</td>
    		@if ($result->hasHost)
    			<td rowspan="2" style="background-color: green; color: yellow">OK</td>
    			<td>Состояние</td>
    			<td>Директива Host указана</td>
				</tr>
				<tr>
					<td>Рекомендации</td>
					<td>Доработки не требуются</td>
				</tr>
    		@else
				<td rowspan="2" style="background-color: red; color: black">ОШИБКА</td>
				<td>Состояние</td>
    			<td>В файле robots.txt не указана директива Host</td>
				</tr>
				<tr>
					<td>Рекомендации</td>
					<td>Программист: Для того, чтобы поисковые системы знали, какая версия сайта является основных зеркалом, необходимо прописать адрес основного зеркала в директиве Host. В данный момент это не прописано. Необходимо добавить в файл robots.txt директиву Host. Директива Host задётся в файле 1 раз, после всех правил.</td>
				</tr>
			@endif

			<tr>
    		<td rowspan="2">8</td>
    		<td rowspan="2">Проверка количества директив Host, прописанных в файле</td>
    		@if ($result->hostCount)
    			<td rowspan="2" style="background-color: green; color: yellow">OK</td>
    			<td>Состояние</td>
    			<td>В файле прописана 1 директива Host</td>
				</tr>
				<tr>
					<td>Рекомендации</td>
					<td>Доработки не требуются</td>
				</tr>
    		@else
				<td rowspan="2" style="background-color: red; color: black">ОШИБКА</td>
				<td>Состояние</td>
    			<td>В файле прописано несколько директив Host</td>
				</tr>
				<tr>
					<td>Рекомендации</td>
					<td>Программист: Директива Host должна быть указана в файле толоко 1 раз. Необходимо удалить все дополнительные директивы Host и оставить только 1, корректную и соответствующую основному зеркалу сайта</td>
				</tr>
			@endif

			<tr>
    		<td rowspan="2">10</td>
    		<td rowspan="2">Проверка размера файла robots.txt</td>
    		@if ($result->sizeCorrect)
    			<td rowspan="2" style="background-color: green; color: yellow">OK</td>
    			<td>Состояние</td>
    			<td>Размер файла robots.txt составляет {{$result->size}} байт, что находится в пределах допустимой нормы</td>
				</tr>
				<tr>
					<td>Рекомендации</td>
					<td>Доработки не требуются</td>
				</tr>
    		@else
				<td rowspan="2" style="background-color: red; color: black">ОШИБКА</td>
				<td>Состояние</td>
    			<td>Размер файла robots.txt составляет {{$result->size}} байт, что превышает допустимую норму</td>
				</tr>
				<tr>
					<td>Рекомендации</td>
					<td>Программист: Максимально допустимый размер файла robots.txt составляем 32 кб. Необходимо отредактировть файл robots.txt таким образом, чтобы его размер не превышал 32 Кб</td>
				</tr>
			@endif

			<tr>
    		<td rowspan="2">11</td>
    		<td rowspan="2">Проверка указания директивы Sitemap</td>
    		@if ($result->hasSitemap)
    			<td rowspan="2" style="background-color: green; color: yellow">OK</td>
    			<td>Состояние</td>
    			<td>Директива Sitemap указана</td>
				</tr>
				<tr>
					<td>Рекомендации</td>
					<td>Доработки не требуются</td>
				</tr>
    		@else
				<td rowspan="2" style="background-color: red; color: black">ОШИБКА</td>
				<td>Состояние</td>
    			<td>В файле robots.txt не указана директива Sitemap</td>
				</tr>
				<tr>
					<td>Рекомендации</td>
					<td>Программист: Добавить в файл robots.txt директиву Sitemap</td>
				</tr>
			@endif

			<tr>
    		<td rowspan="2">12</td>
    		<td rowspan="2">Проверка кода ответа сервера для файла robots.txt</td>
    		@if ($result->responseCorrect)
    			<td rowspan="2" style="background-color: green; color: yellow">OK</td>
    			<td>Состояние</td>
    			<td>Файл robots.txt отдаёт код ответа сервера 200</td>
				</tr>
				<tr>
					<td>Рекомендации</td>
					<td>Доработки не требуются</td>
				</tr>
    		@else
				<td rowspan="2" style="background-color: red; color: black">ОШИБКА</td>
				<td>Состояние</td>
    			<td>При обращении к файлу robots.txt сервер возвращает код ответа {{$result->response}}</td>
				</tr>
				<tr>
					<td>Рекомендации</td>
					<td>Программист: Файл robots.txt должны отдавать код ответа 200, иначе файл не будет обрабатываться. Необходимо настроить сайт таким образом, чтобы при обращении к файлу robots.txt сервер возвращает код ответа 200</td>
				</tr>
			@endif

			@endif
		
    	
    {{-- @foreach($result as $key => $item)
      <tr>
        <td>{{$key}}</td>
        @if ($item == '1')
        <td style="background-color: green; color: yellow">OK</td>
        @elseif($item == '0')      
        <td style="background-color: red; color: black">ERROR</td>
        @else
        <td style="background-color: yellow; color: black">{{$item}}</td>
        @endif
      </tr>
    @endforeach --}}
    </tbody>
  </table>
</div>
<style type="text/css">
	
</style>