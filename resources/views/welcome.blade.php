<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
                flex-direction: column;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            tr{
                max-width: 600px;
            }

        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
                 
                @include('errors')    
                <div>
                    <form action="/analyze" method="POST">
                        {{csrf_field()}}
                      <div class="form-group">
                        <label for="site">Site:</label>
                        <input type="text" class="form-control" id="site" name="url" required>
                      </div>
                      <button type="submit" class="btn btn-success">Submit</button>
                    </form>
                </div>
                <br>  
                <div class="position-ref">
                    @if (!$err)
                        @include('result')
                        <a href="/save" class="btn btn-info" role="button">Download Excel</a>
                    @else
                        {{$err}}
                    @endif
                </div>              
        </div>

    </body>

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        
            $( "#site" ).change(function() {
                let s = $( "#site" ).val();
                if (s.substr(0,7) != 'http://') {
                    $( "#site" ).val('http://' + $( "#site" ).val());
                }
            });
           
    </script>
</html>
